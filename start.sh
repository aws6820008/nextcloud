#!/bin/bash
set -x
yum update -y

#docker
amazon-linux-extra install docker -y
service docker start
systemctl enable docker
usermod -a -G docker ec2-user
chmod 777 /var/run/docker.sock

#docker compose
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# copie yml
curl -o ~/home/ https://gitlab.com/aws6820008/nextcloud/-/blob/main/docker-compose.yml?ref_type=heads

docker-compose up